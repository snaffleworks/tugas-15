@extends('layout.master')
@section('judul')
   List Cast
@endSection
@section('content')

<a href="/cast/create" class="btn btn-success btn-sm mb-2">Tambah</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Bio</th>
      <th scope="col">Umur</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->bio}}</td>
            <td>{{$item->umur}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                <form action="/cast/{{$item->id}}" method="POST">
              
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger btn-sm my-2" value="Delete">
                </form>
            </td>
        </tr> 
        @empty
        
        @endforelse
          
  </tbody>
</table>

@endSection